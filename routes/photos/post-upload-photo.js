module.exports = function(app,mongoose,models){
//uploading the photo
	app.post('/upload',function(req,res){
		if(!req.files){
			return handleError("NO FILES OBJECT");
		} 
		console.log(req.files, req.body.photo);
		var tempPath = req.files.photo.file.path;
		var targetPath = './public/images/uploads/' + req.files.photo.file.name;
		var normalPath = '/images/uploads/' + req.files.photo.file.name;
		var parentGall = req.body.photo.parentmodels.Gallery;
		fs.rename(tempPath,targetPath, function(err){
			if (err){
				handleError(err);
			} 
			console.log("upload succes");
			var newPhoto = new models.Photo({
				parentGalleryId: parentGall,
				filePath:normalPath,
				name:req.body.photo.name
			});
			//insert the final path into the database
			newPhoto.save(function(err, photo){
				if (err){
					handleError(err);
				} 
				console.log("saved photo to db", photo);
				var gallUpdate = {
					$push:{childPhotos: photo._id}
				};
				console.log("Your update is:",gallUpdate);
				//then add that reference to the parent gallery.
				models.Gallery.findByIdAndUpdate(parentGall,gallUpdate,function(err, doc){
					if(err){
						return handleError(err);
					} 
					console.log(doc);
					res.redirect('/browse');
				});
			});
		});
	});
}