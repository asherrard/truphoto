module.exports = function(app,mongoose,models){
//Show the uploaded photos
	app.get('/browse', function(req, res){
		models.Photo.find({},function(err,docs){
			if (err){
				handleError(err);
			} 
			console.log("found some photos", docs);
			res.render('browse',{"data":docs});
		});
	});
}