module.exports = function(app,mongoose,models){
//delete the photo
	app.delete('/photo/:id/delete', function(req,res){
		var photoID = req.params.id;
		models.Photo.findByIdAndRemove(photoID, function(err,photo){
			if(err){
				return errorHandler(err);
			} 
			console.log("removed photo from db", "photo", photo);
			//LEFT OFF HERE NEED TO FIND A WAY TO REMOVE THE PHOTO FROM THE GALLERY WHEN THE PHOTO IS DELETED
										  // db.cpuinfo.update( { flags: 'msr' }, { $pull: { flags: 'msr' } } )
			models.Gallery.findByIdAndUpdate(photo.parentGalleryId, {$pull:{childPhotos:photoID } }, function(err, updatedGallery){
				if(err){
					return handleError(err);	
				} 
				console.log("your updated gallery:",updatedGallery);
				res.send('');
			});
		});
	});
}