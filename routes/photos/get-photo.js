module.exports = function(app,mongoose,models){
	//add a photo
	app.get('/upload',function(req,res){
		models.Gallery.find({},function(err,galleries){
			if(err){
				return handleError(err);
			} 
			res.render('upload', {"data":galleries});
		});
	});
}