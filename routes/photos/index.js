var  getPhoto= require('./get-photo');
var  browsePhotos= require('./get-browse-photos');
var  uploadPhoto= require('./post-upload-photo');
var  deletePhoto= require('./delete-photo');


module.exports.set = function(app,mongoose,models) {
   console.log("set home modules");
   getPhoto(app,mongoose,models);
   browsePhotos(app,mongoose,models);
   uploadPhoto(app,mongoose,models);
   deletePhoto(app,mongoose,models);
};