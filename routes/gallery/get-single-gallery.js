module.exports = function(app,mongoose,models){
//show a single gallery
	app.get('/gallery/:id',function(req,res){
		var galleryID = req.params.id;
		models.Gallery.findById(galleryID).populate("childPhotos").exec(function(err, doc){
			console.log("your doc for rendering galleries",doc);
			if(err){
				return errorHandler(err);
			} 
			res.render('gallery',{"data":doc});
		});
	});
}