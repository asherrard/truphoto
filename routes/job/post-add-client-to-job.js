module.exports = function(app,mongoose,models){
	//add a client to a job
	app.post('/job/:id/client-add',function(req, res){
		var jobID = req.params.id;
		var clientID = req.body.client.id;
		var jobEdits = {
			$push:{childClients:clientID}
		};
		console.log(jobEdits);
		models.Job.findByIdAndUpdate(jobID,jobEdits,function(err){
			if (err){
				return handleError(err);
			} 
			console.log("updated child galleries");
			res.redirect('/adminpanel');
		});
	});
}