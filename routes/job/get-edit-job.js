module.exports = function(app,mongoose,models){
	//edit a job page
	app.get('/job/:id/edit',function(req,res){
		var jobID = req.params.id;
		models.Job.findById(jobID, function(err, doc){
			if(err){
				return errorHandler(err);
			} 
			res.render('job-edit',{"data":doc});
		});
	});
}