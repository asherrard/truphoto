module.exports = function(app,mongoose,models){
	//save job edits
	app.post('/job/:id/edit',function(req, res){
		console.log("trying to save a job edit");
		var jobID = req.params.id;
		var jobEdits = {
			name:req.body.job.name,
			number:req.body.job.number
		};
		models.Job.findByIdAndUpdate(jobID,jobEdits,function(err,doc){
			if(err){
				return handleError(err);
			} 
			console.log("saved job edit");
			res.redirect('/job/'+jobID);
		});
	});
}