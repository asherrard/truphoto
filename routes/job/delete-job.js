module.exports = function(app,mongoose,models){
	//delete a job
	app.delete('/job/:id/delete',function(req,res){
		console.log("trying to delete");
		var jobID = req.params.id;
		models.Job.findByIdAndRemove(jobID, function(err){
			if(err){
				return errorHandler(err);
			} 
			console.log("removed job");
			res.send('');
		});
	});
}