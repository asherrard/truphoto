var getSingleJob = require('./get-single-job');
var createGalleryFromJob = require('./post-create-gallery-from-job');
var editJob = require('./get-edit-job');
var addGalleryToJob = require('./post-add-gallery-to-job');
var addClientToJob = require('./post-add-client-to-job');
var saveEditsToJob = require('./post-save-edits-to-job');
var deleteJob = require('./delete-job');

module.exports.set = function(app,mongoose,models) {
   getSingleJob(app,mongoose,models);
   createGalleryFromJob(app,mongoose,models);
   editJob(app,mongoose,models);
   addGalleryToJob(app,mongoose,models);
   saveEditsToJob(app,mongoose,models);
}