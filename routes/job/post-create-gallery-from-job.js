module.exports = function(app,mongoose,models){
	//add a gallery directly from a job
	app.post('/job/:id/create-gallery',function(req,res){
		var jobID = req.params.id;
		var tags = req.body.gallery.tags.trim();
		var splitTags = tags.split(',');
		var newGall = new models.Gallery({
			name:req.body.gallery.name,
			password:req.body.gallery.password,
			tags: splitTags,
			parentJobs:jobID
		});
		newGall.save(function(err, doc){
			if(err){
				return handleError(err);	
			} 
			console.log("saved gallery", doc);
			var updates = {
				$push:{
					childGalleries:doc._id
				}
			};
			models.Job.findByIdAndUpdate(jobID,updates,function(err,job){
				if(err){
					return handleError(err);
				} 
				console.log(models.Job);
				res.redirect('/job/'+jobID+'/');
			});
		});
		console.log("done posting gallery");
	});
}