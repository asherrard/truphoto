module.exports = function(app,mongoose,models){
	//add a gallery to a job
	app.post('/job/:id/gallery-add',function(req, res){
		var jobID = req.params.id;
		var galleryID = req.body.gallery.id;
		var jobEdits = {
			$push:{childGalleries:galleryID}
		};
		console.log(jobEdits);
		models.Job.findByIdAndUpdate(jobID,jobEdits,function(err){
			if (err){
				return handleError(err);
			} 
			console.log("updated child galleries");
			res.redirect('/adminpanel');
		});
	});
}