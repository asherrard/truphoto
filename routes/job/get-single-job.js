module.exports = function(app,mongoose,models){
	//view single job
	app.get('/job/:id',function(req,res){
		var jobID = req.params.id;
		var dataPayload = {};
		models.Job.findById(jobID).populate('childGalleries childClients').exec(function(err,doc){ //childClients
			if(err) {
				return errorHandler(err);
			}
			dataPayload.job = doc;
			models.Gallery.find({},function(err,galls){
				if(err){
					return errorHandler(err);
				} 
				dataPayload.galleries = galls;
				models.Client.find({},function(err, clients){
					if(err){
						return errorHandler(err);
					} 
					dataPayload.clients = clients;
					console.log(dataPayload);
					res.render('job',{"data":dataPayload});
				});
			});
		}); //http://mongoosejs.com/docs/populate.html
	});
}