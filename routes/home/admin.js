module.exports = function(app,mongoose,models){
	console.log("ran adminpanel");
	app.get('/adminpanel', function(req,res){
		var compoundData = {};
		models.Job.find({},function(err,docs){
			if(err){
				handleError(err);
			}
			else {
				console.log("found jobs");
				compoundData.jobs = docs;
				models.Gallery.find({},function(err,docs){
					if(err){
						handleError(err);
					}
					else {
						compoundData.galleries = docs;
						console.log("found galleries");
						models.Client.find({},function(err,docs){
							if(err){
								return handleError(err);
							}
							console.log("found clients");
							compoundData.clients = docs;
							res.render('adminpanel',{"data":compoundData});
							console.log("loading and rendering adminpanel");
						});
					}
				});
			}
		}); 
		console.log(compoundData);
	});



	app.post('/adminpanel', function(req, res){
		console.log("posting");
		//create a tag array
		if(req.body.gallery){
			var tags = req.body.gallery.tags.trim();
			var splitTags = tags.split(',');
			var newGall = new models.Gallery({
				name:req.body.gallery.name,
				password:req.body.gallery.password,
				tags: splitTags
			});
			newGall.save(function(err){
				if(err) { return handleError(err); }
				console.log("saved gallery");
				res.redirect('/adminpanel');
			});
			console.log("done posting gallery");
		}
		if(req.body.job){
			var newJob = new models.Job({
				name:req.body.job.name,
				number:req.body.job.number
			});
			newJob.save(function(err){
				if(err) { return handleError(err); }
				console.log("saved gallery");
				res.redirect('/adminpanel');
			});
		}
		if(req.body.client){
			var newClient = new models.Client({
				email:req.body.client.email,
				password: req.body.client.password,
				phone:req.body.client.phone,
				firstName:req.body.client.firstName,
				lastName:req.body.client.lastName,
			});
			newClient.save(function(err){
				if(err){ return handleError(err);}
				console.log("saved client");
				res.redirect('/adminpanel');
			});
		}
	});
}