module.exports = function(app,mongoose,models){
	console.log("ran post login");
	app.post('/login', function(req, res){
		//query for that user
		models.User.findOne({
			//grab data from the form
			'email':req.body.user.email,
			'password':req.body.user.password
		},function(err,user){
			if(err){
				throw "That email and password were not found";
			}
			console.log("found that user "+ req.body.user.email+" and password"+req.body.user.password);
			//if found login to admin panel
			if(req.body.user.email === "ADMIN"){
				res.redirect('/adminpanel');
			}
			else {
				res.redirect('/browse');
			}
		});	
	}); //end post login
}