
/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index');
};

exports.adminpanel = function(req, res){
  res.render('adminpanel', {email:"Admin"});
};

var jobs = require('./job');
var galleries = require('./gallery');
var clients = require('./client');
var home = require('./home');
var photos = require('./photos');

module.exports.set = function(app,mongoose,models) {
   jobs.set(app,mongoose,models);
   galleries.set(app,mongoose,models);
   clients.set(app,mongoose,models);
   home.set(app,mongoose,models);
   photos.set(app,mongoose,models);
};