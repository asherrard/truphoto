module.exports = function(app,mongoose,models){
//show a single client
	app.get('/client/:id',function(req,res){
		var clientID = req.params.id;
		models.Client.findById(clientID, function(err, doc){
			if(err){
				return errorHandler(err);
			} 
			res.render('client',{"data":doc});
		});
	});
}