//ajax.js
console.log("loaded ajax functions");
var deleteForm = $('#delete-job-form');
var deleteFormURL = deleteForm.attr('action');
console.log(deleteFormURL);
deleteForm.on('submit',function(e){
	e.preventDefault();
});
$('#delete-job').on('click', function(){
	console.log("clicked to delete");
		if(confirm("Do you want to delete this?")){
			$.ajax({
				url:deleteFormURL,
				type:"DELETE",
				success:function(){
					console.log("deleted!");
					window.location.assign('/adminpanel');
				}
			});
		}
});
$('#photo-gallery img').on('click',function(){
	if(confirm("delete this photo?")){
		var photoId = $(this).attr('title');
		var webserviceURL = '/photo/'+photoId+'/delete';
		$.ajax({
			url:webserviceURL,
			type:"DELETE",
			dataType:"html",
			success:function(){
				console.log("deleted!");
				window.location.assign('/browse');
			}
		});
	}
});
$('#single-job .open-gall-details').on('click',function(e){
	e.preventDefault();
	var linkUrl = $(this).attr('href');
	console.log("URL to hit", linkUrl);
	$.ajax({
		url:linkUrl,
		type:"GET",
		success:function(data){
			console.log("Succesfull ajax", data);
			// $('#modal-container .modal-body').html(data);
			$('#modal-container').modal();
		}
	})

});