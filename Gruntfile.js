/*global module:false*/
module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
      ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',

    uglify: {
      prod: {
        options:{
          mangle:true,
          compress:true,
          report:'min'
        },
        files: {
          'public/js/post.scripts.min.js' : ['public/js/req.scripts.js']
        }
      }
    },

    requirejs: {
      app:{
        options:{
          findNestedDependencies:true,
          optimize: "none",
          baseUrl:  "src/js",
          name:     "mainApp",
          out:      "public/js/req.scripts.js"
        }
      }
    },

    jshint: {
      options: {
        curly:   true,
        devel:   true,
        eqeqeq:  true,
        immed:   true,
        latedef: true,
        newcap:  true,
        noarg:   true,
        sub:     true,
        undef:   false,
        // The unused was annoying
        //unused:  true,
        boss:    true,
        eqnull:  true,
        browser: true,
        jquery:  true,
        smarttabs: true,
      },
      gruntfile: {
        src: 'Gruntfile.js'
      },
      node:{
        src: 'app.js'
      },
      src: ['src/js/**/*.js']
    },

    sass: {
      dist: {
        files: [{
          expand: true,
          cwd: 'src/styles',
          src: ['styles.scss'],
          dest: 'public/stylesheets',
          ext: '.css'
        }]
      }
    },
    nodeunit: {
      all: ['test/*.js']
    },
    watch: {
      gruntfile: {
        files: '<%= jshint.gruntfile.src %>',
        tasks: ['jshint:gruntfile']
      },
      sass: {
        files: 'src/styles/**/*.scss',
        tasks: ['sass']
      },
      jshint: {
          files: 'src/js/**/*.js',
          tasks: ['jshint']
      },
      require: {
        files: 'src/js/**/*.js',
        tasks: ['requirejs']
      }
    }
  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-nodeunit');

  //specific tasks
  grunt.registerTask('build',['requirejs']);
  // Default task.
  grunt.registerTask('default', [ 'jshint','requirejs','uglify:prod','sass']);

};
