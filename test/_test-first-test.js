//my first node unit test
module.exports = {
    //all functions need to start with test so nodeUnit can find them
	setUp:function(callback){ //make sure to keep this camelCased otherwise it wont perform as expected
        this.expectedValue = true; //sending our expected value to the testApp function
        callback();
    },
    //now lets do some real testing on my app
    testApp:function(test){
        test.expect(1);
        test.ok(this.expectedValue, "Your setup function worked");
        test.done(); //all tests are completed
    }
}
