
/**
 * Module dependencies.
 */

var express = require('express');
var routes  = require('./routes');
// var user    = require('./routes/user');
var http    = require('http');
var path    = require('path');
var fs      = require('fs');
var assert  = require('assert');

var mongoose = require('mongoose');
var hbs = require('express3-handlebars');
var app = express();
var models = require('./database-set-up/mongoose-schemas');

//ENVIROMENTS 
// app.set('env','development');
app.set('env','production');

//ERROR Handler
var handleError = function(error){
	throw error;
};

//DB CONNECT


console.log("db conn start");
//ENV Databases
var nodejitsuDB = 'mongodb://nodejitsu_asherrard28:93bsu7pjj800vmkit2uud57mcs@ds045998.mongolab.com:45998/nodejitsu_asherrard28_nodejitsudb5734160875';
var devDB = 'mongodb://localhost/truphoto';

if (app.get('env') === 'production'){
	var DBURL = nodejitsuDB;
}
if (app.get('env') === 'development'){
	var DBURL = devDB;
}


var db = mongoose.connection;
db.on('error', function(){
	throw "could not connect to databse";
});

var init = {
	openRoutes:function() {
		 var allRoutes = require('./routes');
		 allRoutes.set(app,mongoose,models);
	}
};


//on a succesfull connect set up schemas for documents/docs
db.once('open', function(){
	console.log("db conn succes");
	init.openRoutes();
	console.log("opened routes");
	
	//request a gallery via ajax
	app.get('/gallery/:id/edit-via-ajax',function(req,res){
		var galleryID = req.params.id;
		models.Gallery.findById(galleryID,function(err,doc){
			if(err){
				return handleError(err);
			} 
			console.log("preparing data for ajax response");
			res.send(doc);
		});
	});
});
mongoose.connect(DBURL);
console.log("db conn end");

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars',hbs({defaultLayout:'main'}));
app.set('view engine', 'handlebars');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.bodyParser());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));


// development only
if ('development' === app.get('env')) {
  app.use(express.errorHandler());
}

//ROUTES
app.get('/', routes.index);
// app.get('/users', user.list);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
