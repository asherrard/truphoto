
/**
 * Module dependencies.
 */

var express = require('express');
var routes  = require('./routes');
// var user    = require('./routes/user');
var http    = require('http');
var path    = require('path');
var fs      = require('fs');
var assert  = require('assert');

var mongoose = require('mongoose');
var hbs = require('express3-handlebars');
var app = express();
var models = require('./database-set-up/mongoose-schemas');

//ENVIROMENTS 
// app.set('env','development');
app.set('env','production');

//ERROR Handler
var handleError = function(error){
	throw error;
};

//DB CONNECT


console.log("db conn start");
//ENV Databases
var nodejitsuDB = 'mongodb://nodejitsu_asherrard28:93bsu7pjj800vmkit2uud57mcs@ds045998.mongolab.com:45998/nodejitsu_asherrard28_nodejitsudb5734160875';
var devDB = 'mongodb://localhost/truphoto';

if (app.get('env') === 'production'){
	var DBURL = nodejitsuDB;
}
if (app.get('env') === 'development'){
	var DBURL = devDB;
}


var db = mongoose.connection;
db.on('error', function(){
	throw "could not connect to databse";
});

var init = {
	openRoutes:function() {
		 var allRoutes = require('./routes');
		 console.log(allRoutes);
		 // allRoutes.set = function(app,mongoose,models){
		 // 	console.log("setting all routes");
		 	allRoutes.set(app,mongoose,models);
		// }
	}
};


//on a succesfull connect set up schemas for documents/docs
db.once('open', function(){
	console.log("db conn succes");
	init.openRoutes();
	console.log("opened routes");
	

	//attach routes related to DB
	/*app.post('/login', function(req, res){
		//query for that user
		models.User.findOne({
			//grab data from the form
			'email':req.body.user.email,
			'password':req.body.user.password
		},function(err,user){
			if(err){
				throw "That email and password were not found";
			}
			console.log("found that user "+ req.body.user.email+" and password"+req.body.user.password);
			//if found login to admin panel
			if(req.body.user.email === "ADMIN"){
				res.redirect('/adminpanel');
			}
			else {
				res.redirect('/browse');
			}
		});	
	}); //end post login*/

	
	/*app.post('/adminpanel', function(req, res){
		console.log("posting");
		//create a tag array
		if(req.body.gallery){
			var tags = req.body.gallery.tags.trim();
			var splitTags = tags.split(',');
			var newGall = new models.Gallery({
				name:req.body.gallery.name,
				password:req.body.gallery.password,
				tags: splitTags
			});
			newGall.save(function(err){
				if(err) { return handleError(err); }
				console.log("saved gallery");
				res.redirect('/adminpanel');
			});
			console.log("done posting gallery");
		}
		if(req.body.job){
			var newJob = new models.Job({
				name:req.body.job.name,
				number:req.body.job.number
			});
			newJob.save(function(err){
				if(err) { return handleError(err); }
				console.log("saved gallery");
				res.redirect('/adminpanel');
			});
		}
		if(req.body.client){
			var newClient = new models.Client({
				email:req.body.client.email,
				password: req.body.client.password,
				phone:req.body.client.phone,
				firstName:req.body.client.firstName,
				lastName:req.body.client.lastName,
			});
			newClient.save(function(err){
				if(err){ return handleError(err);}
				console.log("saved client");
				res.redirect('/adminpanel');
			});
		}
	});
*/

	/*app.get('/adminpanel', function(req,res){
		var compoundData = {};
		models.Job.find({},function(err,docs){
			if(err){
				handleError(err);
			}
			else {
				console.log("found jobs");
				compoundData.jobs = docs;
				models.Gallery.find({},function(err,docs){
					if(err){
						handleError(err);
					}
					else {
						compoundData.galleries = docs;
						console.log("found galleries");
						models.Client.find({},function(err,docs){
							if(err){
								return handleError(err);
							}
							console.log("found clients");
							compoundData.clients = docs;
							res.render('adminpanel',{"data":compoundData});
							console.log("loading and rendering adminpanel");
						});
					}
				});
			}
		}); 
		console.log(compoundData);
	});*/

	// //view single job
	// app.get('/job/:id',function(req,res){
	// 	var jobID = req.params.id;
	// 	var dataPayload = {};
	// 	models.Job.findById(jobID).populate('childGalleries childClients').exec(function(err,doc){ //childClients
	// 		if(err) {
	// 			return errorHandler(err);
	// 		}
	// 		dataPayload.job = doc;
	// 		models.Gallery.find({},function(err,galls){
	// 			if(err){
	// 				return errorHandler(err);
	// 			} 
	// 			dataPayload.galleries = galls;
	// 			models.Client.find({},function(err, clients){
	// 				if(err){
	// 					return errorHandler(err);
	// 				} 
	// 				dataPayload.clients = clients;
	// 				console.log(dataPayload);
	// 				res.render('job',{"data":dataPayload});
	// 			});
	// 		});
	// 	}); //http://mongoosejs.com/docs/populate.html
	// });

	// //add a gallery directly from a job
	// app.post('/job/:id/create-gallery',function(req,res){
	// 	var jobID = req.params.id;
	// 	var tags = req.body.gallery.tags.trim();
	// 	var splitTags = tags.split(',');
	// 	var newGall = new models.Gallery({
	// 		name:req.body.gallery.name,
	// 		password:req.body.gallery.password,
	// 		tags: splitTags,
	// 		parentJobs:jobID
	// 	});
	// 	newGall.save(function(err, doc){
	// 		if(err){
	// 			return handleError(err);	
	// 		} 
	// 		console.log("saved gallery", doc);
	// 		var updates = {
	// 			$push:{
	// 				childGalleries:doc._id
	// 			}
	// 		};
	// 		models.Job.findByIdAndUpdate(jobID,updates,function(err,job){
	// 			if(err){
	// 				return handleError(err);
	// 			} 
	// 			console.log(models.Job);
	// 			res.redirect('/job/'+jobID+'/');
	// 		});
	// 	});
	// 	console.log("done posting gallery");
	// });

	// //edit a job page
	// app.get('/job/:id/edit',function(req,res){
	// 	var jobID = req.params.id;
	// 	models.Job.findById(jobID, function(err, doc){
	// 		if(err){
	// 			return errorHandler(err);
	// 		} 
	// 		res.render('job-edit',{"data":doc});
	// 	});
	// });


	// //add a gallery to a job
	// app.post('/job/:id/gallery-add',function(req, res){
	// 	var jobID = req.params.id;
	// 	var galleryID = req.body.gallery.id;
	// 	var jobEdits = {
	// 		$push:{childGalleries:galleryID}
	// 	};
	// 	console.log(jobEdits);
	// 	models.Job.findByIdAndUpdate(jobID,jobEdits,function(err){
	// 		if (err){
	// 			return handleError(err);
	// 		} 
	// 		console.log("updated child galleries");
	// 		res.redirect('/adminpanel');
	// 	});
	// });



	// //add a client to a job
	// app.post('/job/:id/client-add',function(req, res){
	// 	var jobID = req.params.id;
	// 	var clientID = req.body.client.id;
	// 	var jobEdits = {
	// 		$push:{childClients:clientID}
	// 	};
	// 	console.log(jobEdits);
	// 	models.Job.findByIdAndUpdate(jobID,jobEdits,function(err){
	// 		if (err){
	// 			return handleError(err);
	// 		} 
	// 		console.log("updated child galleries");
	// 		res.redirect('/adminpanel');
	// 	});
	// });



	// //save job edits
	// app.post('/job/:id/edit',function(req, res){
	// 	console.log("trying to save a job edit");
	// 	var jobID = req.params.id;
	// 	var jobEdits = {
	// 		name:req.body.job.name,
	// 		number:req.body.job.number
	// 	};
	// 	models.Job.findByIdAndUpdate(jobID,jobEdits,function(err,doc){
	// 		if(err){
	// 			return handleError(err);
	// 		} 
	// 		console.log("saved job edit");
	// 		res.redirect('/job/'+jobID);
	// 	});
	// });


	// //delete a job
	// app.delete('/job/:id/delete',function(req,res){
	// 	console.log("trying to delete");
	// 	var jobID = req.params.id;
	// 	models.Job.findByIdAndRemove(jobID, function(err){
	// 		if(err){
	// 			return errorHandler(err);
	// 		} 
	// 		console.log("removed job");
	// 		res.send('');
	// 	});
	// });


	//add a photo
	app.get('/upload',function(req,res){
		models.Gallery.find({},function(err,galleries){
			if(err){
				return handleError(err);
			} 
			res.render('upload', {"data":galleries});
		});
	});


	//uploading the photo
	app.post('/upload',function(req,res){
		if(!req.files){
			return handleError("NO FILES OBJECT");
		} 
		console.log(req.files, req.body.photo);
		var tempPath = req.files.photo.file.path;
		var targetPath = './public/images/uploads/' + req.files.photo.file.name;
		var normalPath = '/images/uploads/' + req.files.photo.file.name;
		var parentGall = req.body.photo.parentmodels.Gallery;
		fs.rename(tempPath,targetPath, function(err){
			if (err){
				handleError(err);
			} 
			console.log("upload succes");
			var newPhoto = new models.Photo({
				parentGalleryId: parentGall,
				filePath:normalPath,
				name:req.body.photo.name
			});
			//insert the final path into the database
			newPhoto.save(function(err, photo){
				if (err){
					handleError(err);
				} 
				console.log("saved photo to db", photo);
				var gallUpdate = {
					$push:{childPhotos: photo._id}
				};
				console.log("Your update is:",gallUpdate);
				//then add that reference to the parent gallery.
				models.Gallery.findByIdAndUpdate(parentGall,gallUpdate,function(err, doc){
					if(err){
						return handleError(err);
					} 
					console.log(doc);
					res.redirect('/browse');
				});
			});
		});
	});



	//delete the photo
	app.delete('/photo/:id/delete', function(req,res){
		var photoID = req.params.id;
		models.Photo.findByIdAndRemove(photoID, function(err,photo){
			if(err){
				return errorHandler(err);
			} 
			console.log("removed photo from db", "photo", photo);
			//LEFT OFF HERE NEED TO FIND A WAY TO REMOVE THE PHOTO FROM THE GALLERY WHEN THE PHOTO IS DELETED
										  // db.cpuinfo.update( { flags: 'msr' }, { $pull: { flags: 'msr' } } )
			models.Gallery.findByIdAndUpdate(photo.parentGalleryId, {$pull:{childPhotos:photoID } }, function(err, updatedGallery){
				if(err){
					return handleError(err);	
				} 
				console.log("your updated gallery:",updatedGallery);
				res.send('');
			});
		});
	});

	//Show the uploaded photos
	app.get('/browse', function(req, res){
		models.Photo.find({},function(err,docs){
			if (err){
				handleError(err);
			} 
			console.log("found some photos", docs);
			res.render('browse',{"data":docs});
		});
	});


	//show a single gallery
	app.get('/gallery/:id',function(req,res){
		var galleryID = req.params.id;
		models.Gallery.findById(galleryID).populate("childPhotos").exec(function(err, doc){
			console.log("your doc for rendering galleries",doc);
			if(err){
				return errorHandler(err);
			} 
			res.render('gallery',{"data":doc});
		});
	});

	//show a single client
	app.get('/client/:id',function(req,res){
		var clientID = req.params.id;
		models.Client.findById(clientID, function(err, doc){
			if(err){
				return errorHandler(err);
			} 
			res.render('client',{"data":doc});
		});
	});


	//request a gallery via ajax
	app.get('/gallery/:id/edit-via-ajax',function(req,res){
		var galleryID = req.params.id;
		models.Gallery.findById(galleryID,function(err,doc){
			if(err){
				return handleError(err);
			} 
			console.log("preparing data for ajax response");
			res.send(doc);
		});
	});
});
mongoose.connect(DBURL);
console.log("db conn end");

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars',hbs({defaultLayout:'main'}));
app.set('view engine', 'handlebars');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.bodyParser());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));


// development only
if ('development' === app.get('env')) {
  app.use(express.errorHandler());
}

//ROUTES
app.get('/', routes.index);
// app.get('/users', user.list);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
