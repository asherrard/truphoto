define([],function(){
var ajax= {
	photos:function(){
		$('#photo-gallery img').on('click',function(){
			if(confirm("delete this photo?")){
				var photoId = $(this).attr('title');
				var webserviceURL = '/photo/'+photoId+'/delete';
				$.ajax({
					url:webserviceURL,
					type:"DELETE",
					dataType:"html",
					success:function(){
						console.log("deleted!");
						window.location.assign('/browse');
					}
				});
			}
		});
	},
	jobsGet:function(){
		$('#single-job .open-gall-details').on('click',function(e){
			e.preventDefault();
			var linkUrl = $(this).attr('href');
			console.log("URL to hit", linkUrl);
			$.ajax({
				url:linkUrl,
				type:"GET",
				success:function(data){
					console.log("Succesfull ajax", data);
					// $('#modal-container .modal-body').html(data);
					$('#modal-container').modal();
				}
			});
		});
	},
	init:function(){
		this.photos();
		this.jobsGet();
	}
};
return ajax;
});