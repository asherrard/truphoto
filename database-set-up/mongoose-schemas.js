//this file sets the schemas for our different collections so that we can use them for querying

var mongoose = require('mongoose');


//we need the schema object
console.log("starting schemas");
var Schema = mongoose.Schema;
//MODELS

//job
var jobSchema = new Schema({
	name:String,
	number: Number,
	childGalleries: [{type: Schema.Types.ObjectId, ref: "Gallery"}],
	childClients: [{type: Schema.Types.ObjectId, ref: "Client"}],
},{
	collection:'jobs'
});
var Job = mongoose.model('Job', jobSchema);
exports.Job = Job;

//user
var userSchema = new Schema({
	email:String,
	password: String
},{
	collection:'users'
});
var User = mongoose.model('User', userSchema);
exports.User = User;

//gallery
var gallerySchema = new Schema({
	name:String,
	description:String,
	isPrivate:Boolean,
	tags:Array,
	parentJobs: [{type: Schema.Types.ObjectId, ref: "Job"}],
	childPhotos:[{type: Schema.Types.ObjectId, ref: "Photo"}]
}, {
	collection:'galleries'
});
var Gallery = mongoose.model('Gallery',gallerySchema);
exports.Gallery = Gallery;

//photo
var photoSchema = new Schema({
	parentGalleryId: String,
	filePath: String,
	name: String
},{
	collection:"photos"
});
var Photo = mongoose.model('Photo',photoSchema);
exports.Photo = Photo;

//client
var clientSchema = new Schema({
	email:String,
	password: String,
	phone:String,
	firstName:String,
	lastName:String,
},{
	collection:'clients'
});
var Client = mongoose.model('Client', clientSchema);
exports.Client = Client;
exports.Index = function(){
	//INDEX FOR QUERYING
	userSchema.index({
		email:1
	});
	gallerySchema.index({
		name:1
	});
	jobSchema.index({
		number:1
	});
	clientSchema.index({
		email:1,
		lastName:1
	});
};
